package com.seapay.restapi.builder;

import com.seapay.restapi.model.Voucher;

import java.math.BigDecimal;

public class VoucherBuilder {
    private BigDecimal discount;
    private BigDecimal cashback;
    private BigDecimal topup;
    private BigDecimal price;
    private String code;
    private String type;

    public VoucherBuilder(){
        discount = new BigDecimal(0);
        cashback = new BigDecimal(0);
        topup = new BigDecimal(0);
    }

    public VoucherBuilder withDiscount(BigDecimal discount){
        this.discount = discount;
        return this;
    }

    public VoucherBuilder withCashback(BigDecimal cashback){
        this.cashback = cashback;
        return this;
    }

    public VoucherBuilder withTopup(BigDecimal topup){
        this.topup = topup;
        return this;
    }

    public VoucherBuilder withCode(String code){
        this.code = code;
        return this;
    }

    public VoucherBuilder withType(String type){
        this.type = type;
        return this;
    }

    public VoucherBuilder withPrice(BigDecimal price){
        this.price = price;
        return this;
    }

    public Voucher build(){
        return new Voucher(this);
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public BigDecimal getCashback() {
        return cashback;
    }

    public BigDecimal getTopup() {
        return topup;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }
}
