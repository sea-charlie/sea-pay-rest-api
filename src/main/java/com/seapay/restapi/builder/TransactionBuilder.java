package com.seapay.restapi.builder;

import com.seapay.restapi.model.Transaction;

import java.math.BigDecimal;

public class TransactionBuilder {
    private String date;
    private String creditedUsername;
    private String debitedUsername;
    private BigDecimal amount;
    private String type;

    public TransactionBuilder withDate(String date){
        this.date = date;
        return this;
    }

    public TransactionBuilder withCreditedUsername(String creditedUsername){
        this.creditedUsername = creditedUsername;
        return this;
    }

    public TransactionBuilder withDebitedUsername(String debitedUsername){
        this.debitedUsername = debitedUsername;
        return this;
    }

    public TransactionBuilder withAmount(BigDecimal amount){
        this.amount = amount;
        return this;
    }

    public TransactionBuilder withType(String type){
        this.type = type;
        return this;
    }

    public Transaction build(){
        return new Transaction(this);
    }

    public String getDate() {
        return date;
    }

    public String getCreditedUsername() {
        return creditedUsername;
    }

    public String getDebitedUsername() {
        return debitedUsername;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }
}
