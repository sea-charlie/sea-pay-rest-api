package com.seapay.restapi.builder;

import com.seapay.restapi.util.Response;
import com.seapay.restapi.util.UserLoginResponse;

public class ResponseBuilder {
    private String service;
    private String message;
    private String data;

    public ResponseBuilder(){
    }

    public ResponseBuilder withService(String service){
        this.service = service;
        return this;
    }

    public ResponseBuilder withMessage(String message){
        this.message = message;
        return this;
    }

    public ResponseBuilder withData(String data){
        this.data = data;
        return this;
    }

    public Response build() {
        return new Response(this);
    }

    public String getService() {
        return service;
    }

    public String getMessage() {
        return message;
    }

    public String getData() {
        return data;
    }


}