package com.seapay.restapi.builder;

import com.seapay.restapi.model.Item;

import java.math.BigDecimal;

public class ItemBuilder {
    private String ownerUsername;
    private String name;
    private BigDecimal price;
    private Long stock;
    private String description;

    public ItemBuilder withOwnerUsername(String username){
        this.ownerUsername = username;
        return this;
    }

    public ItemBuilder withName(String name){
        this.name = name;
        return this;
    }

    public ItemBuilder withPrice(BigDecimal price){
        this.price = price;
        return this;
    }

    public ItemBuilder withStock(Long stock){
        this.stock = stock;
        return this;
    }

    public ItemBuilder withDescription(String description){
        this.description = description;
        return this;
    }

    public Item build(){
        return new Item(this);
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Long getStock() {
        return stock;
    }

    public String getDescription() {
        return description;
    }
}
