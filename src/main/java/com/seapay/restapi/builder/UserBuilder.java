package com.seapay.restapi.builder;

import com.seapay.restapi.model.User;

public class UserBuilder {
    private String username;
    private String password;
    private Boolean adminStatus;
    private String email;
    private String telepon;

    public UserBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withAdminStatus(Boolean adminStatus){
        this.adminStatus = adminStatus;
        return this;
    }

    public UserBuilder withEmail(String email){
        this.email = email;
        return this;
    }

    public UserBuilder withTelepon(String telepon){
        this.telepon = telepon;
        return this;
    }

    public User build() {
        return new User(this);
    }

    //GETTER
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getTelepon() {
        return telepon;
    }

    public Boolean isAdmin(){
        return adminStatus;
    }
}