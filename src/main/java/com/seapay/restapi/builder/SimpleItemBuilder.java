package com.seapay.restapi.builder;

import com.seapay.restapi.util.SimpleItem;

import java.math.BigDecimal;

public class SimpleItemBuilder {
    private Long id;
    private BigDecimal price;
    private Long jumlah;

    public SimpleItemBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public SimpleItemBuilder setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public SimpleItemBuilder setJumlah(Long jumlah) {
        this.jumlah = jumlah;
        return this;
    }

    public SimpleItem createSimpleItem() {
        return new SimpleItem(id, price, jumlah);
    }
}