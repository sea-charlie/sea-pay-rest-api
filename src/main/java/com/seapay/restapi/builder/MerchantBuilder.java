package com.seapay.restapi.builder;

import com.seapay.restapi.model.Merchant;

public class MerchantBuilder {
    private String username;
    private String password;
    private String description;

    public MerchantBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public MerchantBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public MerchantBuilder withDescription(String description){
        this.description = description;
        return this;
    }

    public Merchant build() {
        return new Merchant(this);
    }

    //GETTER
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDescription(){
        return description;
    }
}
