package com.seapay.restapi.util;

import com.seapay.restapi.builder.ResponseBuilder;

public class Response {
    private String service;
    private String message;
    private String data;

    public Response(ResponseBuilder responseBuilder){
        this.service = responseBuilder.getService();
        this.message = responseBuilder.getMessage();
        this.data = responseBuilder.getData();
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}