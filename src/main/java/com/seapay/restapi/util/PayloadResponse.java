package com.seapay.restapi.util;

public class PayloadResponse<T> {
    private String message;
    private T payload;

    public PayloadResponse(String message, T data){
        this.message = message;
        this.payload = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
