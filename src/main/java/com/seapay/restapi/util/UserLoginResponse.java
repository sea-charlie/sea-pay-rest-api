package com.seapay.restapi.util;

import com.seapay.restapi.builder.ResponseBuilder;

public class UserLoginResponse<T> extends Response {
    private String authToken;

    public UserLoginResponse(ResponseBuilder responseBuilder){
        super(responseBuilder);
        //this.authToken = responseBuilder.getAuthToken();
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
