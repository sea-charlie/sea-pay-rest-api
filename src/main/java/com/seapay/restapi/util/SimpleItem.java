package com.seapay.restapi.util;

import java.math.BigDecimal;

public class SimpleItem {
    private Long id;
    private BigDecimal price;
    private Long jumlah;

    public SimpleItem(Long id, BigDecimal price, Long jumlah) {
        this.id = id;
        this.price = price;
        this.jumlah = jumlah;
    }

    public Long getJumlah() {
        return jumlah;
    }

    public void setJumlah(Long jumlah) {
        this.jumlah = jumlah;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
