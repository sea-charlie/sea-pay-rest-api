package com.seapay.restapi.util;

import java.security.MessageDigest;

public class Encryptor {
    public static String encryptPassword(String password) throws Exception{
        // Create MessageDigest instance for MD5
        MessageDigest md = MessageDigest.getInstance("MD5");
        //Password bytes di masukkan
        md.update(password.getBytes());

        //Ambil hasil hash dari bytes passwords
        byte[] bytes = md.digest();
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}