package com.seapay.restapi.controller;

import com.seapay.restapi.builder.ResponseBuilder;
import com.seapay.restapi.model.User;
import com.seapay.restapi.request.ProposalRequest;
import com.seapay.restapi.request.UserRequest;
import com.seapay.restapi.request.VoucherRequest;
import com.seapay.restapi.service.AdminService;
import com.seapay.restapi.service.UserService;
import com.seapay.restapi.util.PayloadResponse;
import com.seapay.restapi.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    AdminService adminService;
    @Autowired
    UserService userService;

    @PutMapping("/login")
    public ResponseEntity<Response> login(@RequestBody UserRequest userRequest) {
        String message = "";
        String data = "";
        HttpStatus httpStatus = null;
        Response response = null;

        try{
            User userFound = userService.findByUsername(userRequest.getUsername());

            if(userFound == null) {
                message = "Gagal login, username tidak ditemukan";
                httpStatus = HttpStatus.BAD_REQUEST;
            }else if(!userFound.isAdmin()){
                message = "Gagal login, username salah!";
                httpStatus = HttpStatus.BAD_REQUEST;
            } else if (!userRequest.checkPassword(userFound.getPassword())){
                message = "Gagal login, password salah";
                httpStatus = HttpStatus.BAD_REQUEST;
            }else{
                data = userService.generateUserToken(userFound.getId());
                message = "Berhasil login";
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e){
            message = e.getMessage();
        }

        response = new ResponseBuilder()
                .withService(this.getClass().getName())
                .withMessage(message)
                .withData(data)
                .build();

        return  ResponseEntity
                .status(httpStatus)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PutMapping("/logout")
    public ResponseEntity<Response> logout(@RequestBody UserRequest userRequest) {
        User userFound = userService.findByUsername(userRequest.getUsername());

        String message = "";
        String data = userRequest.getUsername();
        HttpStatus httpStatus = null;

        if(userFound == null){
            httpStatus = HttpStatus.BAD_REQUEST;
            message = "Gagal memproses logout!";
        } else{
            if(userRequest.checkToken(userFound.getAuthToken())){
                userService.clearUserToken(userFound.getId());
                httpStatus = HttpStatus.OK;
                message = "Berhasil logout";
            } else{
                httpStatus = HttpStatus.BAD_REQUEST;
                message = "Token salah!";
            }
        }

        Response response = new ResponseBuilder()
                .withService(this.getClass().getName())
                .withMessage(message)
                .withData(data)
                .build();

        return  ResponseEntity
                .status(httpStatus)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PutMapping("/accept")
    public PayloadResponse acceptMerchant(@RequestBody ProposalRequest proposalRequest){
        try{
            adminService.acceptProposal(proposalRequest);
            return new PayloadResponse<String>("Sukses!","");
        } catch(Exception e){
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @PutMapping("/decline")
    public PayloadResponse declineMerchant(@RequestBody ProposalRequest proposalRequest){
        try{
            adminService.declineProposal(proposalRequest);
            return new PayloadResponse<String>("Sukses!","");
        } catch(Exception e){
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @PostMapping("/topup-voucher")
    public PayloadResponse addTopupVoucher(@RequestBody VoucherRequest voucherRequest){
        try{
            adminService.addTopupVoucher(voucherRequest);
            return new PayloadResponse<String>("Sukses!","");
        } catch(Exception e){
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @PostMapping("/discount-voucher")
    public PayloadResponse addDiscountVoucher(@RequestBody VoucherRequest voucherRequest){
        try{
            adminService.addDiscountVoucher(voucherRequest);
            return new PayloadResponse<String>("Sukses!","");
        } catch(Exception e){
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @PostMapping("/cashback-voucher")
    public PayloadResponse addCashbackVoucher(@RequestBody VoucherRequest voucherRequest){
        try{
            adminService.addCashbackVoucher(voucherRequest);
            return new PayloadResponse<String>("Sukses!","");
        } catch(Exception e){
            return new PayloadResponse<String>("Error", e.toString());
        }
    }
}
