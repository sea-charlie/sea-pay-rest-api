package com.seapay.restapi.controller;

import com.seapay.restapi.request.PaymentRequest;
import com.seapay.restapi.service.PaymentService;
import com.seapay.restapi.util.PayloadResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    PaymentService paymentService;

    public PaymentController(){
        this.paymentService = new PaymentService();
    }

    @PostMapping("/pay")
    public ResponseEntity<PayloadResponse> pay(@RequestBody PaymentRequest paymentRequest){
        return processPayment(paymentRequest, "pay");
    }

    @PostMapping("/topup")
    public ResponseEntity<PayloadResponse> topup(@RequestBody PaymentRequest paymentRequest){
        return processPayment(paymentRequest, "topup");
    }

    @PostMapping("/transfer")
    public ResponseEntity<PayloadResponse> transfer(@RequestBody PaymentRequest paymentRequest){
        return processPayment(paymentRequest, "transfer");
    }

    private ResponseEntity<PayloadResponse> processPayment(PaymentRequest paymentRequest, String paymentType){
        String message = "";
        HttpStatus httpStatus = null;

        try{
            if(paymentType.equals("pay")){
                paymentService.pay(paymentRequest);
            } else if(paymentType.equals("topup")){
                paymentService.topup(paymentRequest);
            } else if(paymentType.equals("transfer")){
                paymentService.transfer(paymentRequest);
            } else{
                throw new Exception();
            }
            message = "Transaksi berhasil";
            httpStatus = HttpStatus.OK;
        } catch (Exception e){
            message = "Transaksi gagal, " + e.getMessage();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        PayloadResponse response = new PayloadResponse<String>(message, "");

        return  ResponseEntity
                .status(httpStatus)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
}
