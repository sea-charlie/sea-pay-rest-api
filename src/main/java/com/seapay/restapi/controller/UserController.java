package com.seapay.restapi.controller;

import com.seapay.restapi.builder.ResponseBuilder;
import com.seapay.restapi.model.*;
import com.seapay.restapi.request.TokenRequest;
import com.seapay.restapi.request.VoucherRequest;
import com.seapay.restapi.service.UserService;
import com.seapay.restapi.request.UserRequest;
import com.seapay.restapi.util.PayloadResponse;
import com.seapay.restapi.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    public UserController() {
        this.userService = new UserService();
    }

    @PutMapping("/login")
    public ResponseEntity<Response> login(@RequestBody UserRequest userRequest) {
        User userFound = userService.findByUsername(userRequest.getUsername());

        String message = "";
        String data = "";
        HttpStatus httpStatus = null;

        if (userFound == null) {
            message = "Gagal login, username tidak ditemukan";
            httpStatus = HttpStatus.BAD_REQUEST;
        } else if (!userRequest.checkPassword(userFound.getPassword())) {
            message = "Gagal login, password salah";
            httpStatus = HttpStatus.BAD_REQUEST;
        } else {
            try{
                data = userService.generateUserToken(userFound.getId());
                message = "Berhasil login";
                httpStatus = HttpStatus.OK;
            } catch(Exception e){
                message = "Gagal login";
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            }
        }

        Response response = new ResponseBuilder().withService(this.getClass().getName()).withMessage(message)
                .withData(data).build();

        return ResponseEntity.status(httpStatus).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @PutMapping("/logout")
    public ResponseEntity<Response> logout(@RequestBody UserRequest userRequest) {
        User userFound = userService.findByUsername(userRequest.getUsername());

        String message = "";
        String data = userRequest.getUsername();
        HttpStatus httpStatus = null;

        if (userFound == null) {
            httpStatus = HttpStatus.BAD_REQUEST;
            message = "Gagal memproses logout!";
        } else {
            if (userRequest.checkToken(userFound.getAuthToken())) {
                userService.clearUserToken(userFound.getId());
                httpStatus = HttpStatus.OK;
                message = "Berhasil logout";
            } else {
                httpStatus = HttpStatus.BAD_REQUEST;
                message = "Token salah!";
            }
        }

        Response response = new ResponseBuilder().withService(this.getClass().getName()).withMessage(message)
                .withData(data).build();

        return ResponseEntity.status(httpStatus).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    // Create a new Note
    @PostMapping("/register")
    public ResponseEntity<Response> register(@RequestBody UserRequest userRequest) {
        User userFound = userService.findByUsername(userRequest.getUsername());

        String message = "";
        String data = userRequest.getUsername() + " " + userRequest.isAdmin();
        HttpStatus httpStatus = null;

        if (userFound == null) {
            try {
                userService.create(userRequest);
                message = "Berhasil register";
                httpStatus = HttpStatus.OK;
            } catch (Exception e) {
                message = "Gagal register, password bermasalah";
                httpStatus = HttpStatus.BAD_REQUEST;
            }
        } else {
            message = "Gagal register";
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        Response response = new ResponseBuilder().withService(this.getClass().getName()).withMessage(message)
                .withData(data).build();

        return ResponseEntity.status(httpStatus).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @PutMapping("/change-password")
    public ResponseEntity<Response> changePassword(@RequestBody UserRequest userRequest) {
        User userFound = userService.findByUsername(userRequest.getUsername());

        String message = "";
        String data = userRequest.getUsername();
        HttpStatus httpStatus = null;

        if (userFound == null) {
            httpStatus = HttpStatus.BAD_REQUEST;
            message = "Gagal mengganti";
        } else {
            if (userRequest.checkToken(userFound.getAuthToken())) {
                try {
                    userService.updateUserPassword(userFound.getId(), userRequest.getPassword());
                    httpStatus = HttpStatus.OK;
                    message = "Berhasil ganti password";
                } catch (Exception e) {
                    httpStatus = HttpStatus.BAD_REQUEST;
                    message = "Error, invalid password";
                }
            } else {
                httpStatus = HttpStatus.BAD_REQUEST;
                message = "Token salah!";
            }
        }

        Response response = new ResponseBuilder().withService(this.getClass().getName()).withMessage(message)
                .withData(data).build();

        return ResponseEntity.status(httpStatus).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @GetMapping("/view-merchants")
    public PayloadResponse getMerchants() {
        String message = "";
        List<Merchant> data = null;
        HttpStatus httpStatus = null;

        try {
            data = userService.viewMerchants();
            message = "Success";
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            message = "Error";
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        PayloadResponse response = new PayloadResponse<List>("Berhasil!", data);

        return response;
    }

    @GetMapping("/merchant-page/{username}")
    public PayloadResponse getMerchantPage(@PathVariable(value = "username") String username) {
        String message = "";
        List<Item> data = null;
        HttpStatus httpStatus = null;

        try {
            data = userService.viewMerchantPage(username);
            message = "Success";
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            message = e.getMessage();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

       PayloadResponse response = new PayloadResponse<List>(message, data);

        return response;
    }

    @PutMapping("/buy-item")
    public PayloadResponse buyItem(){
        return null;
    }

    @GetMapping("/vouchers")
    public PayloadResponse getVouchers() {
        String message = "";
        List<Voucher> data = null;
        HttpStatus httpStatus = null;

        try {
            data = userService.viewVouchers();
            message = "Success";
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            message = "Error";
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        PayloadResponse response = new PayloadResponse<List>(message, data);

        return response;
    }

    @GetMapping("/history")
    public PayloadResponse getHistory(@RequestHeader(value="token") String token) {
        TokenRequest tokenRequest = new TokenRequest(token);
        User userFound = userService.findByAuthToken(tokenRequest.getToken());

        String message = "";
        List<Transaction> data = null;
        HttpStatus httpStatus = null;

        if (userFound == null) {
            httpStatus = HttpStatus.BAD_REQUEST;
            message = "Token salah!";
        } else {
            try {
                data = userService.viewHistory(tokenRequest);
                httpStatus = HttpStatus.OK;
                message = "Success";
            } catch (Exception e) {
                httpStatus = HttpStatus.BAD_REQUEST;
                message = e.getMessage();
            }
        }

        PayloadResponse response = new PayloadResponse<List>(message, data);

        return response;
    }

    @PutMapping("/buy-voucher")
    public PayloadResponse buyVoucher(@RequestBody VoucherRequest voucherRequest) {
        try {
            String code = userService.buyVoucher(voucherRequest);
            return new PayloadResponse<String>("Sukses membeli voucher!", code);
        } catch (Exception e) {
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @PutMapping("/update-profile")
    public PayloadResponse updateProfile(@RequestBody UserRequest userRequest) {
        try {
            userService.updateUserProfile(userRequest);
            return new PayloadResponse<String>("Sukses!", "");
        } catch (Exception e) {
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @GetMapping("/balance")
    public PayloadResponse getUserBalance(@RequestHeader(value="token") String token) {
        TokenRequest tokenRequest = new TokenRequest(token);
        try {
            BigDecimal balance = userService.getUserBalance(tokenRequest);
            return new PayloadResponse<BigDecimal>("Sukses!", balance);
        } catch (Exception e) {
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @GetMapping("/profile")
    public PayloadResponse getProfile(@RequestHeader(value="token") String token){
        try{
            User user = userService.getProfile(token);
            return new PayloadResponse<User>("Sukses!",user);
        } catch(Exception e){
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    // Get All Notes
    @GetMapping("/getall")
    public List<User> getAllUser() {
        return userService.findAll();
    }
}