package com.seapay.restapi.controller;

import com.seapay.restapi.builder.ResponseBuilder;
import com.seapay.restapi.model.Merchant;
import com.seapay.restapi.request.ItemRequest;
import com.seapay.restapi.request.MerchantRequest;
import com.seapay.restapi.service.MerchantService;
import com.seapay.restapi.util.PayloadResponse;
import com.seapay.restapi.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/merchant")
public class MerchantController{
    @Autowired
    MerchantService merchantService;

    @PutMapping("/login")
    public ResponseEntity<Response> login(@RequestBody MerchantRequest merchantRequest) {
        Merchant merchantFound = merchantService.findByUsername(merchantRequest.getUsername());

        String message = "";
        String data = "";
        HttpStatus httpStatus = null;

        if(merchantFound == null) {
            message = "Gagal login, username tidak ditemukan";
            httpStatus = HttpStatus.BAD_REQUEST;
        }else if (!merchantRequest.checkPassword(merchantFound.getPassword())){
            message = "Gagal login, password salah";
            httpStatus = HttpStatus.BAD_REQUEST;
        }else{
            data = merchantService.generateMerchantToken(merchantFound.getId());
            message = "Berhasil login";
            httpStatus = HttpStatus.OK;
        }

        Response response = new ResponseBuilder()
                .withService(this.getClass().getName())
                .withMessage(message)
                .withData(data)
                .build();

        return  ResponseEntity
                .status(httpStatus)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PutMapping("/logout")
    public ResponseEntity<Response> logout(@RequestBody MerchantRequest merchantRequest) {
        Merchant merchantFound = merchantService.findByUsername(merchantRequest.getUsername());

        String message = "";
        String data = merchantRequest.getUsername();
        HttpStatus httpStatus = null;

        if(merchantFound == null){
            httpStatus = HttpStatus.BAD_REQUEST;
            message = "Gagal memproses logout!";
        } else{
            if(merchantRequest.checkToken(merchantFound.getAuthToken())){
                merchantService.clearMerchantToken(merchantFound.getId());
                httpStatus = HttpStatus.OK;
                message = "Berhasil logout";
            } else{
                httpStatus = HttpStatus.BAD_REQUEST;
                message = "Token salah!";
            }
        }

        Response response = new ResponseBuilder()
                .withService(this.getClass().getName())
                .withMessage(message)
                .withData(data)
                .build();

        return  ResponseEntity
                .status(httpStatus)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PutMapping("/change-password")
    public ResponseEntity<Response> changePassword(@RequestBody MerchantRequest merchantRequest){
        Merchant merchantFound = merchantService.findByUsername(merchantRequest.getUsername());

        String message = "";
        String data = merchantRequest.getUsername();
        HttpStatus httpStatus = null;

        if(merchantFound == null){
            httpStatus = HttpStatus.BAD_REQUEST;
            message = "Gagal mengganti";
        } else{
            if(merchantRequest.checkToken(merchantFound.getAuthToken())){
                try{
                    merchantService.updateMerchantPassword(merchantFound.getId(), merchantRequest.getPassword());
                    httpStatus = HttpStatus.OK;
                    message = "Berhasil ganti password";
                } catch (Exception e){
                    httpStatus = HttpStatus.BAD_REQUEST;
                    message = "Error, invalid password";
                }
            } else{
                httpStatus = HttpStatus.BAD_REQUEST;
                message = "Token salah!";
            }
        }

        Response response = new ResponseBuilder()
                .withService(this.getClass().getName())
                .withMessage(message)
                .withData(data)
                .build();

        return  ResponseEntity
                .status(httpStatus)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    // Create a new Note
    @PostMapping("/register")
    public ResponseEntity<Response> register(@RequestBody MerchantRequest merchantRequest) {
        Merchant merchantFound = merchantService.findByUsername(merchantRequest.getUsername());

        String message = "";
        String data = merchantRequest.getUsername();
        HttpStatus httpStatus = null;

        if(merchantFound == null) {
            try{
                merchantService.create(merchantRequest);
                message = "Berhasil register";
                httpStatus = HttpStatus.OK;
            } catch (Exception e){
                message = "Gagal register, password bermasalah";
                httpStatus = HttpStatus.BAD_REQUEST;
            }
        } else{
            message = "Gagal register";
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        Response response = new ResponseBuilder()
                .withService(this.getClass().getName())
                .withMessage(message)
                .withData(data)
                .build();

        return  ResponseEntity
                .status(httpStatus)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PostMapping("/add-item")
    public PayloadResponse addItem(@RequestBody ItemRequest itemRequest){
        try{
            merchantService.addItem(itemRequest);
            return new PayloadResponse<String>("Sukses!","");
        } catch(Exception e){
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @PutMapping("/update-profile")
    public PayloadResponse updateProfile(@RequestBody MerchantRequest merchantRequest){
        try{
            merchantService.updateMerchantProfile(merchantRequest);
            return new PayloadResponse<String>("Sukses!","");
        } catch(Exception e){
            return new PayloadResponse<String>("Error", e.toString());
        }
    }

    @GetMapping("/getall")
    public List<Merchant> getAllMerchant(){
        return merchantService.findAll();
    }
}
