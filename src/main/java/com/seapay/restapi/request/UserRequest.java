package com.seapay.restapi.request;
import com.google.gson.annotations.SerializedName;
import com.seapay.restapi.util.Encryptor;

public class UserRequest {
    private String username;
    private String password;
    private String name;
    private String email;
    private String telepon;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getTelepon() {
        return telepon;
    }

    //private int admin;
    private String token;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    // UTILS
    public boolean isAdmin() {
        return false;
    }

    public boolean checkToken(String token){
        if(token.equals("")){
            return false;
        } else{
            return token.equals(this.token);
        }
    }

    public String getToken() {
        return token;
    }

    public boolean checkPassword(String password){
        try{
            String encrypted = Encryptor.encryptPassword(this.password);
            return encrypted.equals(password);
        } catch (Exception e){
            return false;
        }
    }
}
