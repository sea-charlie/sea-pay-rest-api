package com.seapay.restapi.request;

import java.math.BigDecimal;

public class VoucherRequest {
    String username;
    String token;
    BigDecimal amount;
    String code;
    BigDecimal price;
    Long id;

    public Boolean checkToken(String token){
        return token.equals(this.token);
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCode() {
        return code;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Long getId() {
        return id;
    }
}
