package com.seapay.restapi.request;

import com.seapay.restapi.util.Encryptor;

public class MerchantRequest {
    private String name;
    private String username;
    private String password;
    private String token;
    private String description;

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public String getDescription() {
        return description;
    }

    public Boolean checkPassword(String password){
        try{
            String encrypted = Encryptor.encryptPassword(this.password);
            return encrypted.equals(password);
        } catch (Exception e){
            return false;
        }
    }

    public Boolean checkToken(String token){
        if(token.equals("")){
            return false;
        } else{
            return token.equals(this.token);
        }
    }
}
