package com.seapay.restapi.request;

import com.sun.org.apache.xpath.internal.operations.Bool;

public class ProposalRequest {
    String username;
    String merchant;
    String token;

    public Boolean checkToken(String token){
        return token.equals(this.token);
    }

    public String getUsername() {
        return username;
    }

    public String getMerchant() {
        return merchant;
    }

    public String getToken() {
        return token;
    }
}
