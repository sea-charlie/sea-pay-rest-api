package com.seapay.restapi.request;

public class TokenRequest {
    //String username;
    String token;

    public TokenRequest(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public Boolean checkToken(String token){
        if(token.equals("")){
            return false;
        } else{
            return token.equals(this.token);
        }
    }
}
