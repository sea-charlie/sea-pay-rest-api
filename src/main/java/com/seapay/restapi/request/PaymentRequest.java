package com.seapay.restapi.request;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.math.BigDecimal;

public class PaymentRequest {
    private String token;
    private String type;
    private BigDecimal amount;
    private String debited;
    private String credited;
    private String voucher;
    private String items;

    public String getItems() {
        return items;
    }

    public String getToken() {
        return token;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDebited() {
        return debited;
    }

    public String getCredited() {
        return credited;
    }

    public PaymentRequest setDebited(String debited){
        this.debited = debited;
        return this;
    }

    public PaymentRequest setCredited(String credited){
        this.credited = credited;
        return this;
    }

    public String getVoucher() {
        return voucher;
    }

    public Boolean checkToken(String token){
        return token.equals(this.token);
    }

    public void decreaseAmount(BigDecimal amount){
        this.amount = this.amount.subtract(amount);
        if((this.amount.compareTo(BigDecimal.ZERO)) < 0){
            this.amount = BigDecimal.ZERO;
        }
    }

    public PaymentRequest withAmount(BigDecimal amount){
        this.amount = amount;
        return this;
    }
}
