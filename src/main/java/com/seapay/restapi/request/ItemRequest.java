package com.seapay.restapi.request;

import java.math.BigDecimal;

public class ItemRequest {
    private String owner;
    private String name;
    private BigDecimal price;
    private Long stock;
    private String description;
    private String token;

    public String getToken() {
        return token;
    }

    public String getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Long getStock() {
        return stock;
    }

    public String getDescription() {
        return description;
    }
}
