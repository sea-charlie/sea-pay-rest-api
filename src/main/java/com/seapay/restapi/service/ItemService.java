package com.seapay.restapi.service;

import com.seapay.restapi.builder.ItemBuilder;
import com.seapay.restapi.model.Item;
import com.seapay.restapi.repository.ItemRepository;
import com.seapay.restapi.request.ItemRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Component
public class ItemService {
    @Autowired
    private ItemRepository itemRepository;

    // CREATE
    public void create(ItemRequest itemRequest){
        Item item = new ItemBuilder()
                .withOwnerUsername(itemRequest.getOwner())
                .withName(itemRequest.getName())
                .withDescription(itemRequest.getDescription())
                .withPrice(itemRequest.getPrice())
                .withStock(itemRequest.getStock())
                .build();
        itemRepository.save(item);
    }

    // READ
    public List<Item> findAll(){
        return itemRepository.findAll();
    }

    public List<Item> findByOwnerUsername(String username){
        return itemRepository.findByOwnerUsername(username);
    }

    public Item findById(Long id){
        return itemRepository.getOne(id);
    }

    public Item findByOwnerUsernameAndId(String username, Long id){
        return itemRepository.findByOwnerUsernameAndId(username, id);
    }

    // UPDATE
    public void save(Item item){
        itemRepository.save(item);
    }

    public void updateStock(Long id, Long newStock){
        Item item = itemRepository.getOne(id);
        item.setStock(newStock);
        itemRepository.save(item);
    }

    public void updatePrice(Long id, BigDecimal newPrice){
        Item item = itemRepository.getOne(id);
        item.setPrice(newPrice);
        itemRepository.save(item);
    }

    public void updateName(Long id, String name){
        Item item = itemRepository.getOne(id);
        item.setName(name);
        itemRepository.save(item);
    }

    public void updateDescription(Long id, String description){
        Item item = itemRepository.getOne(id);
        item.setDescription(description);
        itemRepository.save(item);
    }

    // DELETE
    public void deleteById(Long id){
        itemRepository.deleteById(id);
    }
}
