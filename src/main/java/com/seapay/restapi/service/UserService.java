package com.seapay.restapi.service;

import com.seapay.restapi.builder.UserBuilder;
import com.seapay.restapi.model.*;
import com.seapay.restapi.repository.*;
import com.seapay.restapi.request.TokenRequest;
import com.seapay.restapi.request.UserRequest;
import com.seapay.restapi.request.VoucherRequest;
import com.seapay.restapi.util.Encryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.List;

@Component
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WalletService walletService;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private VoucherRepository voucherRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private ItemRepository itemRepository;

    // CREATE
    public void create(UserRequest userRequest) throws Exception {
        try {
            String password = Encryptor.encryptPassword(userRequest.getPassword());

            User user = new UserBuilder().withUsername(userRequest.getUsername()).withPassword(password)
                    .withAdminStatus(userRequest.isAdmin()).withEmail(userRequest.getEmail())
                    .withTelepon(userRequest.getTelepon()).build();

            // Add "u_" to user wallet username
            walletService.create("u_" + user.getUsername());
            userRepository.save(user);
        } catch (Exception e) {
            throw e;
        }
    }

    // READ
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findById(Long id) {
        return userRepository.getOne(id);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User findByAuthToken(String token) {
        return userRepository.findByAuthToken(token);
    }

    public BigDecimal getUserBalance(TokenRequest tokenRequest) throws Exception {
        try {
            User user = userRepository.findByAuthToken(tokenRequest.getToken());
            Wallet userWallet = walletService.findByUsername("u_" + user.getUsername());
            if (tokenRequest.checkToken(user.getAuthToken())) {
                return userWallet.getBalance();
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public User getProfile(String token) throws Exception{
        return userRepository.findByAuthToken(token);
    }

    // UPDATE
    public void save(User user) {
        userRepository.save(user);
    }

    public void updateUserPassword(Long id, String password) throws Exception {
        try {
            User user = userRepository.getOne(id);
            String new_password = Encryptor.encryptPassword(password);
            user.setPassword(new_password);
            userRepository.save(user);
        } catch (Exception e) {
            throw e;
        }
    }

    public void updateUserProfile(UserRequest userRequest) throws Exception {
        try {
            User user = userRepository.findByUsername(userRequest.getUsername());
            if (userRequest.checkToken(user.getAuthToken())) {
                user.setName(userRequest.getName());
                user.setTelepon(userRequest.getTelepon());
                user.setEmail(userRequest.getEmail());
                userRepository.save(user);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    // DELETE
    public void delete(String username) {
        userRepository.deleteByUsername(username);
        walletService.deleteByUsername("u_" + username);
    }

    // OTHER SERVICES
    public String buyVoucher(VoucherRequest voucherRequest) throws Exception {
        User user = userRepository.findByAuthToken(voucherRequest.getToken());
        Voucher voucher = voucherRepository.getOne(voucherRequest.getId());

        Boolean canBuy = (user.getSeaPoint().compareTo(voucher.getSeaPrice()) >= 0);

        if ((canBuy) && (!voucher.isOwned())) {
            user.decreaseSeaPoint(voucher.getSeaPrice());
            voucher.grantOwner();

            userRepository.save(user);
            voucherRepository.save(voucher);
            return voucher.getCode();
        } else {
            throw new Exception();
        }
    }

    public List<Merchant> viewMerchants() {
        List<Merchant> merchants = merchantRepository.findAll();
        return merchants;
    }

    public List<Item> viewMerchantPage(String merchantUsername) {
        return itemRepository.findByOwnerUsername(merchantUsername) ;
    }

    public List<Voucher> viewVouchers() {
        List<Voucher> vouchers = voucherRepository.findByOwned(false);
        return vouchers;
    }

    public List<Transaction> viewHistory(TokenRequest tokenRequest) {
        User user = userRepository.findByAuthToken(tokenRequest.getToken());
        List<Transaction> trx = transactionRepository.findByCreditedUsername(user.getUsername());
        List<Transaction> trx2 = transactionRepository.findByDebitedUsername(user.getUsername());
        List<Transaction> trx3 = transactionRepository.findByCreditedUsername("u_"+user.getUsername());
        List<Transaction> trx4 = transactionRepository.findByDebitedUsername("u_"+user.getUsername());

        trx.addAll(trx2);
        trx.addAll(trx3);
        trx.addAll(trx4);
        return trx;
    }

    // UTILS
    public String generateUserToken(Long id) throws Exception {
        try{
            SecureRandom random = new SecureRandom();
            byte bytes[] = new byte[20];
            random.nextBytes(bytes);

            String authToken = bytes.toString();
            authToken = Encryptor.encryptPassword(authToken);
            User user = userRepository.getOne(id);
            user.setAuthToken(authToken);
            userRepository.save(user);

            return authToken;
        } catch (Exception e){
            throw e;
        }

    }

    public void clearUserToken(Long id) {
        User user = userRepository.getOne(id);
        user.setAuthToken("");
        userRepository.save(user);
    }
}
