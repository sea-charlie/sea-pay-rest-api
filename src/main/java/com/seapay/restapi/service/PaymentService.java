package com.seapay.restapi.service;

import com.google.gson.Gson;
import com.seapay.restapi.builder.SimpleItemBuilder;
import com.seapay.restapi.builder.TransactionBuilder;
import com.seapay.restapi.model.*;
import com.seapay.restapi.repository.*;
import com.seapay.restapi.request.PaymentRequest;
import com.seapay.restapi.util.SimpleItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.time.format.DateTimeFormatter.ofPattern;

@Component
public class PaymentService {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private WalletService walletService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VoucherRepository voucherRepository;

    // CREATE
    public void createTransaction(PaymentRequest paymentRequest){
        DateTimeFormatter dtf = DateTimeFormatter
                .ofPattern("yyyy/MM/dd HH:mm:ss") ;

        LocalDateTime now = LocalDateTime.now();

        Transaction transaction = new TransactionBuilder()
                .withAmount(paymentRequest.getAmount())
                .withCreditedUsername(paymentRequest.getCredited())
                .withDebitedUsername(paymentRequest.getDebited())
                .withDate(dtf.format(now).toString())
                .withAmount(paymentRequest.getAmount())
                .withType(paymentRequest.getType())
                .build();

//        if(paymentRequest.getType().equals("pay")){
//            String[] items = paymentRequest.getItems().split(",");
//            List<SimpleItem> itemList = new ArrayList<SimpleItem>();
//            for(int i = 0;i < items.length;i++){
//                String[] priceAndAmount = items[i].split("-");
//                SimpleItem simpleItem = new SimpleItemBuilder()
//                        .setId(transaction.getId())
//                        .setPrice(new BigDecimal(Integer.parseInt(priceAndAmount[0])))
//                        .setJumlah(new Long(Integer.parseInt(priceAndAmount[1])))
//                        .createSimpleItem();
//
//                itemList.add(simpleItem);
//            }
//
//            String itemsJson = new Gson().toJson(itemList);
//
//            transaction.setItems(itemsJson);
//        }

        transactionRepository.save(transaction);
    }

    // Payments
    public void topup(PaymentRequest paymentRequest) throws Exception{
        Voucher voucher = voucherRepository.findByCodeAndUsed(paymentRequest.getVoucher(), false);
        if (voucher == null){
            // Invalid voucher
            throw new Exception();
        } else{
            User user = userRepository.findByUsername(paymentRequest.getDebited());
            if(user == null){
                throw new Exception();
            } else if(paymentRequest.checkToken(user.getAuthToken())){
                this.createTransaction(paymentRequest
                .withAmount(voucher.getTopup()));
                Wallet wallet = walletService.findByUsername("u_" + paymentRequest.getDebited());
                wallet.increaseBalance(voucher.getTopup());
                walletService.save(wallet);
            } else{
                throw new Exception();
            }
        }
        voucher.useVoucher();
        voucherRepository.save(voucher);
    }

    public void transfer(PaymentRequest paymentRequest) throws Exception{
        String from = paymentRequest.getCredited();
        String to = paymentRequest.getDebited();
        this.processPayment(paymentRequest, "u_"+from, "u_"+to);
    }

    public void pay(PaymentRequest paymentRequest) throws Exception{
        Boolean isUsingVoucher = false;
        Voucher voucher = null;

        try{
            if(paymentRequest.getVoucher() != null){
                voucher = voucherRepository.findByCodeAndUsed(paymentRequest.getVoucher(), false);
                if(paymentRequest.getVoucher().equals("")){
                    isUsingVoucher = false;
                } else if(voucher == null){
                    // Invalid voucher
                    throw new Exception();
                } else{
                    String type = voucher.getType();

                    if(type.equals("discount")){
                        paymentRequest.decreaseAmount(voucher.getDiscount());
                    } else if(type.equals("cashback")){
                        paymentRequest.decreaseAmount(voucher.getCashback());
                    } else{
                        // Invalid voucher type
                        throw new Exception();
                    }
                    isUsingVoucher = true;
                }
            } //else do nothing

            String from = paymentRequest.getCredited();
            String to = paymentRequest.getDebited();
            this.processPayment(paymentRequest, "u_"+from, "m_"+to);

            if(isUsingVoucher){
                voucher.useVoucher();
                voucherRepository.save(voucher);
            }
        } catch (Exception e){
            throw e;
        }
    }

    public void processPayment(PaymentRequest paymentRequest, String fromWallet, String toWallet) throws Exception{
        User from = userRepository.findByUsername(paymentRequest.getCredited());

        if(from == null){
            // User not found
            throw new Exception("User not found");
        } else if(paymentRequest.checkToken(from.getAuthToken())){
            this.createTransaction(paymentRequest);

            Wallet creditedWallet = walletService.findByUsername(fromWallet);
            Wallet debitedWallet = walletService.findByUsername(toWallet);

            if(creditedWallet.greaterOrEqualThan(paymentRequest.getAmount())){
                debitedWallet.increaseBalance(paymentRequest.getAmount());
                creditedWallet.decreaseBalance(paymentRequest.getAmount());

                walletService.save(creditedWallet);
                walletService.save(debitedWallet);
            } else{
                throw new Exception("Balance kurang");
            }
        } else{
            // Invalid token
            throw new Exception("Token salah");
        }

        try {
            from.increaseLoyaltyPoint(BigDecimal.ONE);
            from.increaseSeaPoint(BigDecimal.ONE);
            if (from.checkLoyaltyLevel()) {
                // Naik level loyalty, dapat bonus berupa tambahan amount
                Wallet userWallet = walletService.findByUsername(from.getUsername());
                userWallet.increaseBalance(new BigDecimal(20000));
                walletService.save(userWallet);
            }
        } catch (Exception e){

        }
        userRepository.save(from);
    }
}
