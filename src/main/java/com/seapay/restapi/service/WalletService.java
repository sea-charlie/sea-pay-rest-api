package com.seapay.restapi.service;

import com.seapay.restapi.model.Wallet;
import com.seapay.restapi.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class WalletService {
    @Autowired
    private WalletRepository walletRepository;

    // CREATE
    public void create(String username){
        walletRepository.save(new Wallet(username));
    }

    // READ
    public List<Wallet> findAll(){
        return walletRepository.findAll();
    }

    public Wallet findByUsername(String username){
        return walletRepository.findByUsername(username);
    }

    public Wallet findById(Long id){
        return walletRepository.getOne(id);
    }
    // UPDATE
    public void save(Wallet wallet){
        walletRepository.save(wallet);
    }

    public void increaseBalance(String username, BigDecimal amount){
        Wallet wallet = walletRepository.findByUsername(username);
        wallet.increaseBalance(amount);
        walletRepository.save(wallet);
    }

    public void decreaseBalance(String username, BigDecimal amount){
        Wallet wallet = walletRepository.findByUsername(username);
        wallet.decreaseBalance(amount);
        walletRepository.save(wallet);
    }

    // DELETE
    public void deleteByUsername(String username){
        walletRepository.deleteByUsername(username);
    }
}
