package com.seapay.restapi.service;

import com.seapay.restapi.builder.ItemBuilder;
import com.seapay.restapi.builder.MerchantBuilder;
import com.seapay.restapi.model.Item;
import com.seapay.restapi.model.Merchant;
import com.seapay.restapi.model.Notification;
import com.seapay.restapi.repository.MerchantRepository;
import com.seapay.restapi.repository.NotificationRepository;
import com.seapay.restapi.request.ItemRequest;
import com.seapay.restapi.request.MerchantRequest;
import com.seapay.restapi.util.Encryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.List;

@Component
public class MerchantService {
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private WalletService walletService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private NotificationRepository notificationRepository;
    // CREATE
    public void create(MerchantRequest merchantRequest) throws Exception{
        try{
            String password = Encryptor.encryptPassword(merchantRequest.getPassword());

            Merchant merchant = new MerchantBuilder()
                    .withUsername(merchantRequest.getUsername())
                    .withPassword(password)
                    .withDescription(merchantRequest.getDescription())
                    .build();

            // Add "m_" to merchant's wallet username
            walletService.create("m_" + merchant.getUsername());
            // Create notification
            Notification notification = new Notification(merchant.getUsername());
            notificationRepository.save(notification);
            merchantRepository.save(merchant);
        } catch (Exception e){
            throw e;
        }
    }

    // READ
    public List<Merchant> findAll(){
        return merchantRepository.findAll();
    }

    public Merchant findByUsername(String username){
        return merchantRepository.findByUsername(username);
    }

    public Merchant findById(Long id){
        return merchantRepository.getOne(id);
    }

    // UPDATE
    public void save(Merchant merchant){
        merchantRepository.save(merchant);
    }

    public void updateMerchantPassword(Long id, String password) throws Exception{
        try{
            Merchant merchant = merchantRepository.getOne(id);
            String new_password = Encryptor.encryptPassword(password);
            merchant.setPassword(new_password);
            merchantRepository.save(merchant);
        } catch (Exception e){
            throw e;
        }
    }

    public void updateMerchantProfile(MerchantRequest merchantRequest) throws Exception{
        try{
            Merchant merchant = merchantRepository.findByUsername(merchantRequest.getUsername());
            if(merchantRequest.checkToken(merchant.getAuthToken())){
                merchant.setName(merchantRequest.getName());
                merchant.setDescription(merchantRequest.getDescription());
                merchantRepository.save(merchant);
            } else{
                throw new Exception();
            }
        } catch (Exception e){
            throw e;
        }
    }

    public void changeAcceptance(String username, Boolean accepted){
        Merchant merchant = merchantRepository.findByUsername(username);
        merchant.setAccepted(accepted);
        merchantRepository.save(merchant);
    }

    // DELETE
    public void deleteByUsername(String username){
        merchantRepository.deleteByUsername(username);
        walletService.deleteByUsername("m_" + username);
    }

    // OTHER SERVICES
    public void addItem(ItemRequest itemRequest) throws Exception{
        Merchant merchant = merchantRepository.findByUsername(itemRequest.getOwner());

        if((merchant.checkToken(itemRequest.getToken())) && (merchant.getAccepted())){
            Item item = new ItemBuilder()
                    .withOwnerUsername(itemRequest.getOwner())
                    .withName(itemRequest.getName())
                    .withDescription(itemRequest.getDescription())
                    .withStock(itemRequest.getStock())
                    .withPrice(itemRequest.getPrice())
                    .build();
            itemService.save(item);
        } else{
            throw new Exception();
        }
    }

    // UTILS
    public String generateMerchantToken(Long id){
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);

        String authToken = bytes.toString();
        Merchant merchant = merchantRepository.getOne(id);
        merchant.setAuthToken(authToken);
        merchantRepository.save(merchant);

        return authToken;
    }

    public void clearMerchantToken(Long id){
        Merchant merchant = merchantRepository.getOne(id);
        merchant.setAuthToken("");
        merchantRepository.save(merchant);
    }
}
