package com.seapay.restapi.service;

import com.seapay.restapi.builder.VoucherBuilder;
import com.seapay.restapi.model.Merchant;
import com.seapay.restapi.model.Notification;
import com.seapay.restapi.model.User;
import com.seapay.restapi.model.Voucher;
import com.seapay.restapi.repository.NotificationRepository;
import com.seapay.restapi.repository.VoucherRepository;
import com.seapay.restapi.request.ProposalRequest;
import com.seapay.restapi.request.VoucherRequest;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Component
public class AdminService {
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private VoucherRepository voucherRepository;
    @Autowired
    private UserService userService;

    // FOR MERCHANT
    public List<Notification> findAllNotification(){
        return notificationRepository.findAll();
    }

    private Boolean isValidAdmin(ProposalRequest proposalRequest) throws Exception{
        User user = userService.findByUsername(proposalRequest.getUsername());
        if(user == null){
            throw new Exception();
        } else{
            return (proposalRequest.checkToken(user.getAuthToken()) && (user.isAdmin()));
        }
    }

    @Transactional
    public void acceptProposal(ProposalRequest proposalRequest) throws Exception{
        try{
            if(isValidAdmin(proposalRequest)){
                notificationRepository.deleteByMerchantUsername(proposalRequest.getMerchant());
                merchantService.changeAcceptance(proposalRequest.getMerchant(), true);
            } else{
                throw new Exception();
            }
        } catch (Exception e){
            throw e;
        }
    }

    @Transactional
    public void declineProposal(ProposalRequest proposalRequest) throws Exception{
        try{
            if(isValidAdmin(proposalRequest)){
                notificationRepository.deleteByMerchantUsername(proposalRequest.getMerchant());
                merchantService.deleteByUsername(proposalRequest.getMerchant());
            }
        } catch (Exception e){
            throw e;
        }
    }

    // FOR VOUCHER
    private Boolean isValidAdmin(VoucherRequest voucherRequest) throws Exception{
        User user = userService.findByUsername(voucherRequest.getUsername());
        if(user == null){
            throw new Exception();
        } else{
            return (voucherRequest.checkToken(user.getAuthToken()) && (user.isAdmin()));
        }
    }

    public void addCashbackVoucher(VoucherRequest voucherRequest) throws Exception{
        try{
            if(isValidAdmin(voucherRequest)){
                Voucher voucher = new VoucherBuilder()
                        .withCode(voucherRequest.getCode())
                        .withCashback(voucherRequest.getAmount())
                        .withType("cashback")
                        .withPrice(voucherRequest.getPrice())
                        .build();
                voucherRepository.save(voucher);
            }
        } catch (Exception e){
            throw e;
        }
    }

    public void addDiscountVoucher(VoucherRequest voucherRequest) throws Exception{
        try{
            if(isValidAdmin(voucherRequest)){
                Voucher voucher = new VoucherBuilder()
                        .withCode(voucherRequest.getCode())
                        .withDiscount(voucherRequest.getAmount())
                        .withType("discount")
                        .withPrice(voucherRequest.getPrice())
                        .build();
                voucherRepository.save(voucher);
            }
        } catch (Exception e){
            throw e;
        }
    }

    public void addTopupVoucher(VoucherRequest voucherRequest) throws Exception{
        try{
            if(isValidAdmin(voucherRequest)){
                Voucher voucher = new VoucherBuilder()
                        .withCode(voucherRequest.getCode())
                        .withTopup(voucherRequest.getAmount())
                        .withType("topup")
                        .withPrice(voucherRequest.getPrice())
                        .build();
                voucherRepository.save(voucher);
            }
        } catch (Exception e){
            throw e;
        }
    }
}
