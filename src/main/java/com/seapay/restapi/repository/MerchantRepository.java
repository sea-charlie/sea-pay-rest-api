package com.seapay.restapi.repository;

import com.seapay.restapi.model.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant,Long> {
    public Merchant findByUsername(String username);
    public void deleteByUsername(String username);
}
