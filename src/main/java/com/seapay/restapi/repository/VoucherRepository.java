package com.seapay.restapi.repository;

import com.seapay.restapi.model.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoucherRepository extends JpaRepository<Voucher, Long> {
    public Voucher findByCodeAndUsed(String code, Boolean used);

    public List<Voucher> findByOwned(Boolean owned);
}
