package com.seapay.restapi.repository;

import com.seapay.restapi.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {
    public Wallet findByUsername(String username);
    public void deleteByUsername (String username);
}
