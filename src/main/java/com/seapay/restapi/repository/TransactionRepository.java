package com.seapay.restapi.repository;

import com.seapay.restapi.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    public List<Transaction> findByCreditedUsername(String creditedUsername);
    public List<Transaction> findByDebitedUsername(String debitedUsername);
}
