package com.seapay.restapi.repository;

import com.seapay.restapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public User findByUsername(String username);

    public User findByAuthToken(String token);

    public void deleteByUsername(String username);
}