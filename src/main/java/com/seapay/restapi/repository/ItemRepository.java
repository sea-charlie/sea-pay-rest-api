package com.seapay.restapi.repository;

import com.seapay.restapi.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long>{
    public List<Item> findByOwnerUsername(String ownerUsername);
    public Item findByOwnerUsernameAndId(String ownerUsername, Long id);
}
