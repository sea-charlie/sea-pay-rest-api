package com.seapay.restapi.exception;

public class UserNotFoundException extends Exception {
    private String username;
    public UserNotFoundException(String username) {
        super(String.format("Account is not found with username : '%s'", username));
    }
}