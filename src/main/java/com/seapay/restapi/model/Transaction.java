package com.seapay.restapi.model;

import com.seapay.restapi.builder.TransactionBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "transactions")
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;
    private String date;
    private String creditedUsername;
    private String debitedUsername;
    private BigDecimal amount;
    private String type;
    private String items;

    public Transaction(){
        super();
    }

    public Transaction(TransactionBuilder transactionBuilder){
        this.date = transactionBuilder.getDate();
        this.creditedUsername = transactionBuilder.getCreditedUsername();
        this.debitedUsername = transactionBuilder.getDebitedUsername();
        this.amount = transactionBuilder.getAmount();
        this.type = transactionBuilder.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreditedUsername() {
        return creditedUsername;
    }

    public void setCreditedUsername(String creditedUsername) {
        this.creditedUsername = creditedUsername;
    }

    public String getDebitedUsername() {
        return debitedUsername;
    }

    public void setDebitedUsername(String debitedUsername) {
        this.debitedUsername = debitedUsername;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }
}
