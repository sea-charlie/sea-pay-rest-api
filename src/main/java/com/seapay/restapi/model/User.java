package com.seapay.restapi.model;

import com.seapay.restapi.builder.UserBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String username;
    @NotBlank
    private String password;

    private BigDecimal seaPoint;
    private BigDecimal loyaltyPoint;
    private Integer loyaltyLevel;
    private Boolean adminStatus;
    private String authToken;
    private BigDecimal checkPoint;
    private String name;
    private String email;
    private String telepon;

    public User(){
        super();
    }

    public User(UserBuilder userBuilder) {
        super();
        this.username = userBuilder.getUsername();
        this.password = userBuilder.getPassword();
        this.adminStatus = userBuilder.isAdmin();

        this.email = userBuilder.getEmail();
        this.telepon = userBuilder.getTelepon();

        this.seaPoint = new BigDecimal(0);
        this.loyaltyPoint = new BigDecimal(0);
        this.loyaltyLevel = 0;
        this.checkPoint = new BigDecimal(0);

        authToken = "";
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public BigDecimal getSeaPoint() {
        return seaPoint;
    }

    public BigDecimal getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public Integer getLoyaltyLevel() {
        return loyaltyLevel;
    }

    public Boolean isAdmin() {
        return adminStatus;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void increaseLoyaltyPoint(BigDecimal loyaltyPoint){
        this.loyaltyPoint = this.loyaltyPoint.add(loyaltyPoint);
    }

    public void increaseSeaPoint(BigDecimal seaPoint){
        this.seaPoint = this.seaPoint.add(seaPoint);
    }

    public void decreaseSeaPoint(BigDecimal seaPoint){
        this.seaPoint = this.seaPoint.subtract(seaPoint);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public Boolean checkLoyaltyLevel(){
        BigDecimal increment = loyaltyPoint.subtract(checkPoint);
        if(increment.compareTo(BigDecimal.TEN) >= 0){
            checkPoint = checkPoint.add(BigDecimal.TEN);
            loyaltyLevel = loyaltyLevel + 1;
            return true;
        } else{
            return false;
        }
    }
}