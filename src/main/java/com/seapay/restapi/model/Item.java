package com.seapay.restapi.model;

import com.seapay.restapi.builder.ItemBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "items")
public class Item {
    @Id
    @GeneratedValue
    private Long id;
    private String ownerUsername;
    private String name;
    private BigDecimal price;
    private Long stock;
    private String description;

    public Item(){
        super();
    }

    public Item(ItemBuilder itemBuilder){
        this.ownerUsername = itemBuilder.getOwnerUsername();
        this.name = itemBuilder.getName();
        this.price = itemBuilder.getPrice();
        this.stock = itemBuilder.getStock();
        this.description = itemBuilder.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
