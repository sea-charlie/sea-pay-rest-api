package com.seapay.restapi.model;

import com.seapay.restapi.builder.MerchantBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "merchants")
public class Merchant {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String username;
    @NotBlank
    private String password;

    private Boolean accepted;
    private String name;
    private String description;
    private String authToken;

    public Merchant(){
        super();
    }

    public Merchant(MerchantBuilder merchantBuilder) {
        super();
        this.username = merchantBuilder.getUsername();
        this.password = merchantBuilder.getPassword();
        this.description = merchantBuilder.getDescription();
        this.accepted = false;
        this.name = this.username;

        authToken = "";
    }

    public Boolean checkToken(String token){
        if(token.equals("")){
            return false;
        } else{
            return token.equals(this.authToken);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
