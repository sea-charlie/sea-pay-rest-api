package com.seapay.restapi.model;

import com.seapay.restapi.builder.VoucherBuilder;
import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "vouchers")
public class Voucher {
    @Id
    @GeneratedValue
    private Long id;
    private BigDecimal discount;
    private BigDecimal cashback;
    private BigDecimal topup;
    private String code;
    private String type;
    private Boolean used;
    private Boolean owned;
    private BigDecimal seaPrice;

    public BigDecimal getSeaPrice() {
        return seaPrice;
    }

    public Voucher(){
        super();
    }

    public Voucher(VoucherBuilder voucherBuilder){
        this.discount = voucherBuilder.getDiscount();
        this.cashback = voucherBuilder.getCashback();
        this.topup = voucherBuilder.getTopup();
        this.code = voucherBuilder.getCode();
        this.type = voucherBuilder.getType();
        this.used = false;
        this.owned = false;
    }

    public Boolean isUsed(){
        return used;
    }

    public Boolean isOwned() {
        return owned;
    }

    public void grantOwner(){
        this.owned = true;
    }

    public void useVoucher(){
        this.used = true;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public BigDecimal getCashback() {
        return cashback;
    }

    public BigDecimal getTopup() {
        return topup;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }


}
