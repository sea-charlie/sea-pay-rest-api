package com.seapay.restapi.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "wallets")
public class Wallet {
    @Id
    @GeneratedValue
    private Long id;
    private String username;
    private BigDecimal balance;

    public Wallet(){
        super();
    }

    public Wallet(String username){
        this.username = username;
        this.balance = new BigDecimal(0);
    }

    public void increaseBalance(BigDecimal amount){
        balance = balance.add(amount);
    }

    public void decreaseBalance(BigDecimal amount){
        balance = balance.subtract(amount);
    }

    public BigDecimal getBalance(){
        return balance;
    }

    public Boolean greaterOrEqualThan(BigDecimal amount){
        return (this.balance.compareTo(amount) >= 0);
    }
}
