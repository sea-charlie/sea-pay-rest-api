package com.seapay.restapi.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notifications")
public class Notification {
    @Id
    @GeneratedValue
    private Long id;
    private String merchantUsername;

    public Notification(){
        super();
    }

    public Notification(String merchantUsername){
        this.merchantUsername = merchantUsername;
    }
}
