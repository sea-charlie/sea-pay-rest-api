# SEA-Pay RESTful API

Dibangun dengan SpringBoot

## Cara Menjalankan

- Pada cmd, ketik `mvn clean install spring-boot:run`
- Jika sudah pernah dijalankan, ketik `mvn spring-boot:run`
- Base URL pada `localhost:8089/sea-pay/`

## Setup Database

- Buka file `/src/main/resources/application.properties`
- Setting bagian ini sesuai konfigurasi database Anda

```
spring.datasource.url = jdbc:mysql://localhost/seapay?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
spring.datasource.username=hendpraz_seapay
spring.datasource.password=passwseapay
```

Keterangan:

- Nama database seapay
- Username: hendpraz_seapay
- Password: passwseapay

## Deployment

- Jalankan:

```
mvn clean heroku:deploy
```

## Menggunakan API

- API digunakan dengan mengirim request kepada endpoint tertentu.
- API akan mengirimkan response berdasarkan request
- Request yang dikirim dalam format JSON, harap cantumkan header:

```
Headers:
{
    Content-Type : application/json
}
```

P.S. : Semua object request yang ditampilkan di bawah tidak boleh null, kecuali jika ditulis "OPTIONAL"

### 1. User Services (/user/\*\*)

#### Endpoint user/login

URL : localhost:8089/sea-pay/user/login

Send request dengan method **PUT** kepada URL berupa JSON yang berisi

```
    Body :
    {
        "username" : "username123",
        "password" : "password123"
    }
```

Response akan berisi authToken pada object "data"

```
    {
        "service": "com.seapay.restapi.controller.AccountController",
        "message": "Berhasil login",
        "data": "iniauthtoken"
    }
```

#### Endpoint user/register

URL : localhost:8089/sea-pay/user/register

Send request dengan method **POST** kepada URL berupa JSON yang berisi

```
    Body :
    {
        "username" : "username123",
        "password" : "password123",
        "email" : "email@hoster.com",
        "telepon" : "081234567890"
    }
```

Response akan berisi informasi hasil proses register (berhasil/tidak).

#### Endpoint user/logout

URL : localhost:8089/sea-pay/user/logout

Send request dengan method **PUT** kepada URL berupa JSON yang berisi username dan token

```
    Body :
    {
        "username" : "username123",
        "token" : "iniauthtoken"
    }
```

Response akan berisi hasil proses logout

#### Endpoint user/balance

URL : localhost:8089/sea-pay/user/balance

Send request dengan method **GET** kepada URL berupa JSON yang berisi token

```
    Headers :
    {
        "token" : "iniauthtoken"
    }
```

Response akan berisi:

```
    Body :
    {
        "message" : "Berhasil mengambil balance",
        "payload" : 150000
    }
```

#### Endpoint user/change-password

URL : localhost:8089/sea-pay/user/change-password

Send request dengan method **PUT** kepada URL berupa JSON yang berisi username, passwordbaru, dan token

```
    Body :
    {
        "username" : "username123",
        "password" : "inipasswordbaru",
        "token" : "iniauthtoken"
    }
```

Response akan berisi hasil proses ganti password

#### Endpoint user/view-merchants/

URL : localhost:8089/sea-pay/user/view-merchants/

Request dengan method **GET** kepada URL.

Response berisi:

```
    Body :
    {
        "message" : "Berhasil"
        "payload" :
            [
                {   "merchant" : "jakartagaming123",
                    "description" : "Jual kursi gaming"
                },
                {   "name" : "masker123",
                    "description" : "Jual masker dan skincare"
                }
                (dst)
            ]
    }
```

#### Endpoint user/merchant-page/{merchant_username}

URL : localhost:8089/sea-pay/user/merchant-page/{merchant_username}

Request dengan method **GET** kepada URL.

Response berisi:

```
    Body :
    {
        "message" : "Berhasil"
        "payload" :
            [
                {   "name" : "item1",
                    "price" : 35000,
                    "stock" : 2,
                    "description" : "item dengan nomor 1"
                },
                {   "name" : "item2",
                    "price" : 55000,
                    "stock" : 5,
                    "description" : "item dengan nomor 2"
                }
            ]
    }
```

#### Endpoint user/vouchers/

URL : localhost:8089/sea-pay/user/vouchers/

Request dengan method **GET** kepada URL.

Response berisi:

```
    Body :
    {
        "message" : "Berhasil"
        "payload" :
            [
                {   "id" : 1,
                    "type" : "discount"
                    "amount" : 50000,
                    "price" : 10
                },
                {   "id" : 2,
                    "type" : "cashback",
                    "amount" : 100000,
                    "price" : 15,
                }
                (dst)
            ]
    }
```

#### Endpoint user/buy-voucher/

Request dengan method **PUT** kepada URL, dengan body:

```
    Body :
    {
        "username" : "username123",
        "token" : "iniauthtoken",
        "id" : 1
    }
```

Keterangan : id adalah id voucher
<br />
Response akan berisi code dari voucher yang dibeli

```
    Body :
    {
        "message" : "Sukses membeli voucher!"
        "payload" : "INIKODEVOUCHER"
    }
```

#### Endpoint user/history/

URL : localhost:8089/sea-pay/user/history/

Satu page akan berisi maksimal 10 transaction history.
<br />
Request dengan method **GET** kepada URL dengan headers

```
    Headers :
    {
        "token" : "iniauthtoken"
    }
```

Response berisi:

```
    Body :
    {
        "message" : "Berhasil"
        "payload" :
            [
                {   "date" : "2019/08/17 11:00:00",
                    "credited" : "username123",
                    "debited" : "merchant123",
                    "amount" : 150000,
                    "type" : "pay"
                },
                {   "date" : "2019/08/17 11:00:00",
                    "credited" : "",
                    "debited" : "username123",
                    "amount" : 100000,
                    "type" : "topup"
                }
            ]
    }
```

#### Endpoint /user/update-profile

URL : localhost:8089/sea-pay/user/update-profile

Request dengan method **PUT** kepada URL dengan body:

```
    Body :
    {
        "username" : "username123",
        "token" : "iniauthtoken",
        "name" : "Hendry Prasetya",
        "email" : "hendpraz@rocketmail.com",
        "telepon" : "081234567890"
    }
```

Response akan berisi sukses/tidaknya proses update profile

### 2. Admin Services (/admin/\*\*)

#### Endpoint /admin/login

URL : localhost:8089/sea-pay/admin/login

Send request dengan method **PUT** kepada URL berupa JSON yang berisi **username admin dan password admin**.

```
    Body :
    {
        "username" : "username123",
        "password" : "password123"
    }
```

Response akan berisi authToken pada object "data"

```
    {
        "service": "com.seapay.restapi.controller.AccountController",
        "message": "Berhasil login",
        "data": "iniauthtoken"
    }
```

#### Endpoint /admin/logout

URL : localhost:8089/sea-pay/admin/logout

Send request dengan method **PUT** kepada URL berupa JSON yang berisi **username admin dan token admin**.

```
    Body :
    {
        "username" : "username123",
        "token" : "iniauthtoken"
    }
```

#### Endpoint /admin untuk Proposal

Pilih salah satu URL tergantung accept atau decline proposal dari merchant:

URL1 : localhost:8089/sea-pay/admin/accept
<br />
URL2 : localhost:8089/sea-pay/admin/decline

Send request dengan method **PUT** kepada URL berupa JSON yang berisi **username admin, username merchant, dan token admin**.

```
    Body :
    {
        "username" : "admin123",
        "merchant" : "merchant123",
        "token" : "iniauthtokenadmin"
    }
```

#### Endpoint /admin untuk voucher

Pilih salah satu jenis voucher yang ingin ditambahkan ke sistem:

URL1 : localhost:8089/sea-pay/admin/topup-voucher
<br />
URL2 : localhost:8089/sea-pay/admin/cashback-voucher
<br />
URL3 : localhost:8089/sea-pay/admin/discount-voucher

Send request dengan method **POST** kepada URL berupa JSON yang berisi **username admin, token admin, amount dan code**.

```
    Body :
    {
        "username" : "admin123",
        "token" : "iniauthtokenadmin",
        "amount" : 50000,
        "code" : "ABCDE123",
        "type" : "cashback"
    }
```

### 3. Merchant Services (/merchant/\*\*)

Request dengan method **POST** pada ketiga URL di bawah dengan body request sama seperti untuk user

URL1 : localhost:8089/sea-pay/merchant/login
<br />
URL2 : localhost:8089/sea-pay/merchant/logout
<br />
URL3 : localhost:8089/sea-pay/merchant/change-password

#### Endpoint /merchant/register

URL : localhost:8089/sea-pay/merchant/register

Send request dengan method **POST** kepada URL berupa JSON yang berisi **username merchant, password merchant, dan description merchant**.

```
    Body :
    {
        "username" : "username123",
        "password" : "password123",
        "description" : "Bandung, Jawa Barat. Jual PC gaming."
    }
```

#### Endpoint /merchant/add-item

URL : localhost:8089/sea-pay/merchant/add-item

Send request dengan method **POST** kepada URL berupa JSON yang berisi **username merchant, password merchant, dan description merchant**.

```
    Body :
    {
        "owner" : "username123",
        "token" : "initokenmerchant",
        "name" : "nama_barang",
        "price" : 35000,
        "stock" : 10,
        "description" : "deskripsi barang"
    }
```

#### Endpoint /merchant/update-profile

URL : localhost:8089/sea-pay/merchant/update-profile

Request dengan method **PUT** kepada URL dengan body:

```
    Body :
    {
        "username" : "username123",
        "token" : "iniauthtoken",
        "name" : "Jakarta Gaming",
        "description" : "Menjual PC gaming"
    }
```

### 4. Payment Services (/payment/\*\*)

URL1 : localhost:8089/sea-pay/payment/pay
<br />
URL2 : localhost:8089/sea-pay/payment/topup
<br />
URL3 : localhost:8089/sea-pay/payment/transfer

Send request dengan method **POST** kepada URL berupa JSON yang berisi **username admin, token admin, amount dan code**.

```
    Body :
    {
        "token" : "iniauthtokensyahrul",
        "type" : "transfer"
        "amount" : 150000,
        "credited" : "syahrul123,
        "debited" : "hendry123"
    }
```

```
    Body :
    {
        "token" : "iniauthtokenhendry",
        "type" : "topup"
        "amount" : 50000,
        "debited" : "hendry123",
        "voucher" : "ABCDEFGH"
    }
```

```
    Body :
    {
        "token" : "iniauthtokenhendry",
        "type" : "pay"
        "amount" : 50000,
        "credited" : "hendry123",
        "debited" : "jakartashop123",
        "voucher" : "OPTIONAL",
        "items" : "1-4,2-3,5-1"
    }
```

Keterangan:

- pay untuk membayar ke merchant
- topup untuk isi saldo
- transfer untuk transfer saldo ke customer lain
- Pada `items`, polanya adalah `id-jumlah,id-jumlah,dst.` 